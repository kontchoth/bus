import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { SearchService } from './../../services/search.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import { map, take, takeUntil } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  private destroyers$: ReplaySubject<boolean> = new ReplaySubject();
  loading: Observable<boolean>;
  loadingPrediction: Observable<boolean>;
  error: Observable<string>;
  routes: Observable<any[]>;
  stops: Observable<any[]>;
  directions: string[] = [];
  predictions: Observable<any[]>;
  trips: Observable<any[]>;
  searchCompleted = false;


  form = new FormGroup({
    route: new FormControl(null, [Validators.required]),
    stop: new FormControl(null, [Validators.required]),
    direction: new FormControl(null, [Validators.required])
  });
  model = {};
  fields: FormlyFieldConfig[] = [];

  constructor(private searchService: SearchService) { }

  ngOnInit(): void {
    this.loading = this.searchService.loading;
    this.error = this.searchService.error;
    this.routes = this.searchService.routes;
    this.stops = this.searchService.stops;
    this.predictions = this.searchService.predictions;
    this.trips = this.searchService.trips;
    this.loadingPrediction = this.searchService.loadingPredictions;

    this.searchService.fetchRoute();

  }

  fetchStops(evt: any): void {
    // Settings the directions
    this.directions = evt.value.attributes.direction_names;
    this.searchService.fetchStops(evt.value.id);
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    const formValue = this.form.value;
    this.searchService.fetchPredictions(formValue.route.id, formValue.stop.id, formValue.direction);
    this.searchCompleted = true;
  }

  ngOnDestroy(): void {
    this.destroyers$.next(false);
    this.destroyers$.complete();
  }

}
