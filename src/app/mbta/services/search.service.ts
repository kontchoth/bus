import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private errorSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private routesSubject: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  private stopsSubject: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  private loadingPredictionSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private predictionsSubject: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  private tripssSubject: BehaviorSubject<any> = new BehaviorSubject<any>([]);


  get loading(): Observable<boolean> {
    return this.loadingSubject.asObservable();
  }

  get loadingPredictions(): Observable<boolean> {
    return this.loadingPredictionSubject.asObservable();
  }

  get error(): Observable<string> {
    return this.errorSubject.asObservable();
  }

  get routes(): Observable<any[]> {
    return this.routesSubject.asObservable();
  }

  get stops(): Observable<any[]> {
    return this.stopsSubject.asObservable();
  }

  get predictions(): Observable<any[]> {
    return this.predictionsSubject.asObservable();
  }

  get trips(): Observable<any[]> {
    return this.tripssSubject.asObservable();
  }


  constructor(private http: HttpClient) {
  }

  fetchRoute(): void {
    this.loadingSubject.next(true);
    this.errorSubject.next(null);
    this.http.get('https://api-v3.mbta.com/routes', {
      headers: new HttpHeaders().set('x-api-key', environment.apiKey),
      params: {
        type: '0,1'
      }
    }).subscribe(
      (response: any) => {
        this.routesSubject.next(response.data);
        this.loadingSubject.next(false);
      },
      (error: any) => {
        this.loadingSubject.next(false);
        this.errorSubject.next(error.message);
      }
    );
  }

  fetchStops(routeId: string): void {
    this.loadingSubject.next(true);
    this.errorSubject.next(null);
    this.http.get('https://api-v3.mbta.com/stops', {
      headers: new HttpHeaders().set('x-api-key', environment.apiKey),
      params: {
        route: routeId
      }
    }).subscribe(
      (response: any) => {
        console.log(response);
        this.stopsSubject.next(response.data);
        this.loadingSubject.next(false);
      },
      (error: any) => {
        this.loadingSubject.next(false);
        this.errorSubject.next(error.message);
      }
    );
  }

  fetchPredictions(routeId: string, stopId: string, direction: string): void {
    this.loadingPredictionSubject.next(true);
    this.errorSubject.next(null);
    this.http.get('https://api-v3.mbta.com/predictions', {
      headers: new HttpHeaders().set('x-api-key', environment.apiKey),
      params: {
        route: routeId,
        stop: stopId,
        direction_id: direction
      }
    }).subscribe(
      (response: any) => {
        console.log(response);
        this.predictionsSubject.next(response.data);
        this.loadingPredictionSubject.next(false);
      },
      (error: any) => {
        this.loadingPredictionSubject.next(false);
        this.errorSubject.next(error.message);
      }
    );
  }

  fetchTrips(routeId: string, stopId: string, direction: string): void {
    this.loadingSubject.next(true);
    this.errorSubject.next(null);
    this.http.get('https://api-v3.mbta.com/trips', {
      headers: new HttpHeaders().set('x-api-key', environment.apiKey),
      params: {
        route: routeId,
        direction_id: direction,
        date: new Date().toLocaleDateString('YYYY-MM-DD')
      }
    }).subscribe(
      (response: any) => {
        console.log(response);
        this.tripssSubject.next(response.data);
        this.loadingSubject.next(false);
      },
      (error: any) => {
        this.loadingSubject.next(false);
        this.errorSubject.next(error.message);
      }
    );
  }
}
